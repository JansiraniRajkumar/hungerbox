package com.example.hungerbox.service;

import java.util.List;

import com.example.hungerbox.dto.OrderItemsListResponseDto;
import com.example.hungerbox.model.Order;

public interface OrderHistoryService {
	
	public List<OrderItemsListResponseDto> getByEmployeeId(long employeeId);
	public List<Order> employeeTransaction(long employeeId);

}
